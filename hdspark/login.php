<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="login.css" rel="stylesheet" type="text/css" />
    <link rel="icon"
          type="image/png"
          href="favicon-16x16.png" />
</head>
<body>

<?php
session_start();

require "user.class.php";
require "hdfs.class.php";

$user = new User();

if($user->isLoggedIn())
    $user->redirectTo('main');

if(isset($_POST["btn-login"])){
    if($user->login($_POST["username"], $_POST["password"])) {
        $user->redirectTo('main');
    }else{
        echo "<script>alert('Username and/or password is incorrect');</script>";
    }
}
?>

<div id="login-form">
    <form method="post">
        <table >
            <tr>
                <td><input type="text" name="username" placeholder="Your Username" required /></td>
            </tr>
            <tr>
                <td><input type="password" name="password" placeholder="Your Password" required /></td>
            </tr>
            <tr>
                <td><button type="submit" name="btn-login">Sign In</button></td>
            </tr>
        </table>
    </form>
</div>
<?php include "footer.html" ?>
</body>
</html>