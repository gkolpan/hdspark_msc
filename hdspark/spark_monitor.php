<?php
session_start();

require_once "spark.class.php";
require_once "user.class.php";

$user = new User();
if(!$user->isLoggedIn())
    $user->redirectTo('login');

$spark = new Spark();
?>

<link rel="stylesheet" type="text/css" href="monitor.css" />

<div id="monitor" >
    <h4>Select App</h4>
    <select id="selectapp" onchange="changeMonitorData(this.value);" class="selector"  size="5" >
        <?php
        $apps = $spark->getUserApps();
        foreach($apps as $app) {
            echo "<option value='" . $app["appid"] . "'>" . $app["name"];
            for($i=strlen($app["appid"]); $i<100; ++$i)
                echo "&nbsp;";
            echo $app["date"] . "</option>";
        }
        ?>
    </select>

    <?php
    if(isset($_POST["appid"])){
        if(strpos($_POST["appid"], "failed") === false){
            $data = $spark->getAppDetails($_POST["appid"]);
    ?>
    <h4>Status</h4>
    <table id="statustb" class="table table-bordered table-striped table-condensed sortable">
        <thead>
        <tr>
            <th>App-Id</th>
            <th>Name</th>
            <th>Start-Time</th>
            <th>End-Time</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <a id="appid" href="http://sparkmaster:18080/history/<?php echo $_POST["appid"] ?>/jobs/"><?php echo $_POST["appid"]; ?></a>
                <?php if($data["status"] != 'FINISHED'){ ?><span id="killapp"><a onclick="killApp('<?php echo $_POST["appid"]; ?>');" href="#">(kill)</a></span><?php }?>
            </td>
            <td><?php echo $data["name"]; ?></td>
            <td><?php echo $data["startTime"]; ?></td>
            <td><?php echo $data["endTime"]; ?></td>
            <td><?php echo $data["status"]; ?></td>
        </tr>
        </tbody>
    </table>
    <h4>Stages  </h4>
    <table id="stagestb" class="table table-bordered table-striped table-condensed sortable">
        <thead>
        <tr>
            <th>Stage</th>
            <th>Stage-Name</th>
            <th>Status</th>
            <th>Input-Records</th>
            <th>Output-Records</th>
            <th>Shuffle-Read</th>
            <th>Shuffle-Write</th>
            <th>Running Time</th>
        </tr>
        </thead>
        <tbody>
        <?php
        for($i=0; $i<count($data["stages"]); $i++)
            {
            ?>
        <tr>
            <td><?php echo $data["stages"][$i]["stageId"]; ?></td>
            <td><?php echo $data["stages"][$i]["name"]; ?></td>
            <td><?php echo $data["stages"][$i]["status"]; ?></td>
            <td class="numtd"><?php echo $data["stages"][$i]["inputRecords"]; ?></td>
            <td class="numtd"><?php echo $data["stages"][$i]["outputRecords"]; ?></td>
            <td class="numtd"><?php echo $data["stages"][$i]["shuffleReadRecords"]; ?></td>
            <td class="numtd"><?php echo $data["stages"][$i]["shuffleWriteRecords"]; ?></td>
            <td class="numtd"><?php echo $data["stages"][$i]["time"] / 1000; ?>sec</td>
        </tr>
        <?php } ?>
        </tbody>
    </table>

    <?php } //to if toy failed ?>

    <button id="console_button" class="assignbut" onclick="showTerminal('<?php echo $_POST["appid"]; ?>');">Show terminal</button>
    <?php if(strpos($_POST["appid"], 'failed') !== false){?><span id="killapp"><a onclick="killApp('<?php echo $_POST["appid"]; ?>');" href="#">(kill)</a></span><?php }?>
</div>
<div id="logo"> <img id="sparkimg" src="images/spark-logo.png"></div>


<div id="window" hidden="hidden">
    <div id="toolbar">
        <div class="top">
            <div id="lights">
                <div class="light red" onclick="showTerminal()">
                    <div class="glyph">×</div>
                    <div class="shine"></div>
                    <div class="glow"></div>
                </div>

            </div>
            <div id="title">
                <div class="folder">
                    <div class="tab"></div>
                    <div class="body"></div>
                </div>
                Spark
            </div>
            <div id="bubble">
                <div class="shine"></div>
                <div class="glow"></div>
            </div>
        </div>
    </div>
    <div id="consolebody">
        <p class="console-info">Loading...</p>
        <div class="cursor"></div>
    </div>
</div>
        <?php }?>
