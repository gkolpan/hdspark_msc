<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="login.css" rel="stylesheet" type="text/css" />
    <title></title>

</head>
<body>
<?php

require "user.class.php";

$user = new User();

if($user->isLoggedIn())
    $user->redirectTo('main');

if(isset($_POST["submit"])) {
    if($_POST["password"] != $_POST["repassword"])
        echo "<script>alert('Passwords do not match!!!');</script>";
    else if($user->register($_POST["username"], $_POST["password"]))
        $user->redirectTo('main');
    else
        echo "<script>alert('Username already exists...');</script>";
}
?>
<div id="login-form">
    <form method="post" action="register.php">
        <table >
            <tr>
                <td><input type="text" name="username" placeholder="Your Username" required /></td>
            </tr>
            <tr>
                <td><input type="password" name="password" placeholder="Your Password" required /></td>
            </tr>
            <tr>
                <td><input type="password" name="repassword" placeholder="Retype Password" required /></td>
            </tr>
            <tr>
                <td><button type="submit" name="submit">Register Now</button></td>
            </tr>
        </table>
    </form>
</div>

<?php include "footer.html" ?>

</form>
</body>
</html>