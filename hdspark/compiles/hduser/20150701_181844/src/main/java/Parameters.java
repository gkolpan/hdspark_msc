package spark;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStreamReader;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//Diavazoume tis parametrous ekteleshs tou programmatos

/**
 * A parameters class with static variables for use throughout the program
 * Read an xml file with the parameters 
 */
public class Parameters
{
	public static String Areas;
	public static String Partitions;
	public static String Input;
	public static String Output;
	public static int SplitSize;
	public static String SplitMethod;

	public Parameters(String XMLPath)
	{
		try
		{
			// File fXmlFile = new File(XMLPath);

			Path pt=new Path(XMLPath);
	                FileSystem fs = pt.getFileSystem(new Configuration());

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = null;
			Document doc = null;

			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fs.open(pt));

			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("parameters");

			Node nNode = nList.item(0);
			if (nNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element) nNode;
				Parameters.Areas = eElement.getElementsByTagName("areas").item(0).getTextContent();
				Parameters.Partitions = eElement.getElementsByTagName("partitions").item(0).getTextContent();
				Parameters.Input = eElement.getElementsByTagName("input").item(0).getTextContent();
				Parameters.Output = eElement.getElementsByTagName("output").item(0).getTextContent();
				Parameters.SplitSize = Integer.parseInt(eElement.getElementsByTagName("split").item(0).getTextContent());
				Parameters.SplitMethod = eElement.getElementsByTagName("splitmethod").item(0).getTextContent();
			}
		}
		catch (Exception ex)
		{
			System.out.println("Parameters file could not be read or corrupted!");
			System.exit(1);
		}
	}

}
