package spark;

import java.io.IOException;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.conf.Configuration;

/**
 *Exei oles tis plhrofories enos partition apo poia perioxh pros poia perioxh kai pou apothikeuetai sto HDFS
 */
/**
 * A node in the partition tree
 * Holds two AreaNode's: from-to
 * Holds the path of the data for the partition
 */
public final class PartitionNode
{
	public AreaNode from;
	public AreaNode to;
	public Path path;

	public PartitionNode(AreaNode from, AreaNode to)
	{
		this.from = from;
		this.to = to;
		if(Parameters.Output.charAt(Parameters.Output.length() - 1) != '\\' && Parameters.Output.charAt(Parameters.Output.length() - 1) != '/' )
			Parameters.Output += '/';
		
		this.path = new Path(Parameters.Output + "(" + from.code + ")-(" + to.code + ")");
	}

	/**
	 * Gets the file size of the partition from the underlying file system
	 */
	public long getFileSize() throws IOException
	{
		FileSystem fs = this.path.getFileSystem(new Configuration());
		if(fs.exists(this.path))
			return fs.getContentSummary(this.path).getLength();
		else
			return 0;
	}
	
	/**
	 * toString() method
	 * Needed for debugging
	 */
	@Override
	public String toString(){
		return "(" + from.code + ") - (" + to.code + ")";
	}
	
}
