package spark;

import java.util.List;

//Perixei ena komvo sto dentro me tis diathesimes perioxes

/**
 * A node in the tree of areas.
 * Defines "area" and "code" for the specific area and holds a list
 * of all the sub-areas.
 */
public class AreaNode
{
	public String area;
	public String code;
	public List<AreaNode> subareas = null;
	
	public AreaNode(String area, String code, List<AreaNode> subareas)
	{
		this.area = area;
		this.code = code;
		this.subareas = subareas;
	}
	
	public AreaNode(String area, String code)
	{
		this.area = area;
		this.code = code;
	}
}
