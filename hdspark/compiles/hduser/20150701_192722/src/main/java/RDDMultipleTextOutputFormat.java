package spark;

import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RecordWriter;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Progressable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat;
import org.apache.hadoop.io.NullWritable;

//Bohthikh gia na apothikeuoume 1 pairRDD me vash to KEY.

/**
 * A class that extends MultipleTextOutputFormat from Hadoop API
 * in order to write a JavaPairRDD to multiple files named by the key.
 * Writes only the value of the pair (not the key) to the output file 
 */
public class RDDMultipleTextOutputFormat<K, V> extends MultipleTextOutputFormat<K,V>
{	
	
	protected String generateFileNameForKeyValue(K key, V value,String name)
	{
		return key.toString();
    }
	
    protected K generateActualKey(K key, V value)
    {
	    return (K) NullWritable.get();
    }
}