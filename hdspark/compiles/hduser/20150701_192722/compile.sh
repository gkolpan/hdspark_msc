mkdir -p src/main/java
mv *.java src/main/java
if [ ! -f pom.xml ]; then
	cp ../../pom.xml .
fi
mvn clean package
