<?php
/**
 * Created by PhpStorm.
 * User: mitsakos
 * Date: 6/17/15
 * Time: 9:15 AM
 */

$servername = "localhost";
$serveruser = "php_hduser";
$serverpassword = "hdsparkuser";

// Create connection
$conn = new mysqli($servername, $serveruser, $serverpassword);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->select_db('web_hdspark');

?>