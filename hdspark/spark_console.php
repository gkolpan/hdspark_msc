<?php

session_start();

require "user.class.php";
require "config.php";

if(!isset($_POST["appid"]))
    throw new RuntimeException("There is no app id to show... :(");

$user = new User();
if(!$user->isLoggedIn())
    $user->redirectTo('login');

$sql = $conn->prepare("SELECT `jobs`.`folder` FROM `web_hdspark`.`jobs` WHERE `jobs`.`appid` = ?;");

if(!$sql)
    throw new RuntimeException("Unable to create query get jobs");

$sql->bind_param("s", $_POST["appid"]);

$res = $sql->execute();

if (!$res)
    throw new RuntimeException('Unable to run query get users...');

$sql->store_result();
$sql->bind_result($folder);
$sql->fetch();

$file = $folder . '/nohup.out';
exec("cat $file | tail -n 10000", $data);
foreach($data as $row){
    echo "<p class='console-";
    if(strpos($row, "WARN") !== false)
        echo "warning'>";
    else if(strpos($row, 'ERROR') !== false)
        echo "error'>";
    else if(strpos($row, 'INFO') !== false)
        echo "info'>";
    else
        echo "none'>";

    echo $row . "</p>";
}

$sql->close();
$conn->close();


?>