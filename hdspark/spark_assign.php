<?php

session_start();

require_once "spark.class.php";
require_once "user.class.php";

$user = new User();
if(!$user->isLoggedIn())
    $user->redirectTo('login');

$spark = new Spark();
$quota = $spark->getUserQuotas();
$spark->getUserJars();
?>

<div id="logo"> <img id="sparkimg" src="images/spark-logo.png"></div>

<div class="assign" id="assign">
    <table>
        <tr><td colspan="3"><h1>Assign Job</h1></td></tr>
        <tr>
            <td>
                <label>Total Cores: </label>
            </td>
            <td colspan="2">
                <input id="core_selector" type="range" min="0" max="<?php echo $quota["cores"]?>" value="<?php echo $quota["cores"]?>" onmousemove="updateTextInput(this);" onchange="updateTextInput(this);" ><label><?php echo $quota["cores"]?></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Memory per Node: </label>
            </td>
            <td colspan="2">
                <input id="memory_selector" type="range" step="512" min="0" max="<?php echo $quota["memory"]?>" value="<?php echo $quota["memory"]?>" onmousemove="updateTextInput(this);" onchange="updateTextInput(this);"><label><?php echo $quota["memory"]?></label>mb
            </td>
        </tr>
        <tr>
            <td>
                <label>JAR executable:</label>
            </td>
            <td>
                <select id="jar_selector">
                    <?php foreach($spark->getUserJars() as $jar){?>
                        <option><?php echo $jar; ?></option>
                    <?php }?>
                </select>
            </td>
            <td>
                <label for="uploadjar"><img src="images/uploadbtn.jpg"></label>
                <input type="file" id="uploadjar" onchange="uploadJar()" hidden="hidden" accept=".jar">
            </td>

        </tr>
        <tr>
            <td>
                <label>Command line parameters:</label>
            </td>
            <td colspan="2">
                <input class="assignforms" id="cmdline_parameters" type="text" placeholder="hdfs://sparkmaster:9000/user/hduser/panos/parameters.xml">
            </td>
        </tr>
        <tr>
            <td>
                <label>Main Class:</label>
            </td>
            <td colspan="2">
                <input class="assignforms" id="main_class" type="text" placeholder="spark.Main">
            </td>
        </tr>
        <tr>
            <td colspan="3" id="assignbut">
                <input  class="assignbut" type="button" value="Fire" onclick="runJob()">
            </td>
        </tr>
    </table>
</div>
