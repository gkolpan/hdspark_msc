<?php

class User{
    public $username = null;
    public $id = null;
    public $level = null;

    function __construct(){
        if(isset($_SESSION['id']) && $_SESSION['id'] != null){
            $this->username = $_SESSION['username'];
            $this->id = $_SESSION['id'];
            $this->level = $_SESSION['level'];
        }
    }

    function register($username, $password){
        include "config.php";

        $sql = $conn->prepare("INSERT INTO `users` (`username`, `password`, `level`) VALUES (?, ?, 1);");

        if(!$sql)
            echo "conn failed";

        $hashed = md5($password);

        $sql->bind_param("ss", $username, $hashed);

        $res = $sql->execute();

        if(!$res) {
            return false;
        }
        $sql->close();

        return true;
    }

    function login($username, $password){
        include "config.php";

        $sql = $conn->prepare("SELECT `id`, `username`, `level` FROM `users` WHERE `username` = ? AND `password` = ? ;");
        if(!$sql)
            throw new RuntimeException("Unable to create query get users");

        $hashed = md5($password);
        $sql->bind_param("ss", $username, $hashed);
        $res = $sql->execute();

        if (!$res)
            throw new RuntimeException('Unable to run query get users...');

        $sql->store_result();
        $sql->bind_result($this->id, $this->username, $this->level);
        $sql->fetch();

        $sql->close();
        $conn->close();

        if ($this->id == null) {
            return false;
        } else {
            $_SESSION['id'] = $this->id;
            $_SESSION['level'] = $this->level;
            $_SESSION['username'] = $this->username;

            return true;
        }

    }

    function isLoggedIn(){
        if($this->id != null)
            return true;
        else
            return false;
    }

    function logOut(){
        // If they are logged in
        if( $this->id != null ){
            // Unset the session variables
            unset($_SESSION['id']);
            unset($_SESSION['level']);
            unset($_SESSION['username']);

            $this->id = null;
            $this->username = null;
            $this->level = null;
            // Redirect to the login page
            $this->redirectTo( 'signout' );
        }
    }

    function redirectTo($page) {
        if( !headers_sent() ){
            header( 'Location: ' . $page . '.php' );
        }
        die( '<a href="'.$page.'.php">Go to '.$page.'.php</a>' );
    }
}

?>