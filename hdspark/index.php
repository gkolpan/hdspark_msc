<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<?php

session_start();

require "user.class.php";
require "hdfs.class.php";
require "spark.class.php";

$user = new User();

if(isset($_POST["logout"])){
    if($user->isLoggedIn())
        $user->logOut();

    return;
}

if(!$user->isLoggedIn())
    $user->redirectTo('login');

$hdfs = new Hdfs();
$spark = new Spark();
if($user->level == 0) {
    //echo json_encode($hdfs->listDirectory('/'), JSON_UNESCAPED_SLASHES);
}else {
    //echo "Periexomeno user";
    //echo json_encode($hdfs->listDirectory('/user/hduser'));
    //$path = 'compiles/hduser/test1';
    //echo var_dump($spark->compile($path));

}

?>

</body>
</html