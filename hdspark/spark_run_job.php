<?php

session_start();

require_once "spark.class.php";
require_once "user.class.php";
try {
    $user = new User();
    if (!$user->isLoggedIn())
        $user->redirectTo('login');

    if (!isset($_POST["memory"]) || !isset($_POST["cores"]) || !isset($_POST["class"]) || !isset($_POST["params"]) || !isset($_POST["jar"]))
        throw new RuntimeException("Not all parameters set...");

    $spark = new Spark();

    $parameters = array();
    if ($_POST["memory"] == 0 || $_POST["cores"] == 0)
        throw new RuntimeException("Memory and cores cant be zero...");

    $parameters["memory"] = $_POST["memory"];
    $parameters["cores"] = $_POST["cores"];
    $parameters["class"] = $_POST["class"];
    $parameters["userparams"] = 'hdfs://sparkmaster:9000/user/hduser/panos/parameters.xml';

    $result = array();

    $id = $spark->run($_POST["jar"], $parameters);
    $result["Result"] = "ok";
    $result["id"] = $id;
}   catch (RuntimeException $ex) {
    $result["Result"] = "failed";
    $result["Error"] = $ex->getMessage();
}

echo json_encode($result);
?>
