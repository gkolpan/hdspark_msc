<?php
/**
 * Created by PhpStorm.
 * User: mitsakos
 * Date: 6/20/15
 * Time: 6:09 PM
 */
require "hdfs.class.php";

$hdfs = new Hdfs();

if(isset($_GET["file"]))
    $path = $_GET["file"];
else
    exit(1);

try {
    $hdfs->downloadFile($path);
}catch(RuntimeException $ex){
    echo $ex->getMessage();
}
?>