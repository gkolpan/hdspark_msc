<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="dist/themes/default/style.css" />
    <link rel="icon"
          type="image/png"
          href="favicon-16x16.png" />

    <script src="script.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="dist/jstree.min.js"></script>
    <title>Signout</title>
</head>
<body>

<div id="logback">
    <a href="login.php"><button class="assignbut">Login</button></a>
</div>
<div class="photos">

    <img src="images/egaleo.jpg" alt="egaleo" >
</div>
<div class="photos">
    <img src="images/geraks" alt="gerakas">
</div>
</body>
</html>