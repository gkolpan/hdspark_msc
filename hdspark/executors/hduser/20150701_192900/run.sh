#!/bin/bash
nohup /usr/local/spark/bin/spark-submit --master spark://sparkmaster:7077 --executor-memory $1 --total-executor-cores $2 --class $3 $4 $5 >nohup.out 2>&1 &
echo $!

