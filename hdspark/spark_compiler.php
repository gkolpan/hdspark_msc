<link rel="stylesheet" type="text/css" href="monitor.css" />


<div id="logo"> <img id="sparkimg" src="images/spark-logo.png">
</div>
<div class="assign" id="compiler">
    <table>
        <tr><td colspan="2"><h1>Compiler Run</h1></td></tr>
        <tr>
            <td>
                <label>Upload Source Files for Compiling:</label>
            </td>
            <td>
                <label for="uploadsource">
                    <img src="images/uploadbtn.jpg">
                    <input type="file" id="uploadsource" onchange="updateUploadJava();" hidden="hidden" accept=".java" multiple>
                </label>
            </td>
            <td rowspan="3">
                <label id="javafiles" style="position: absolute; left: 0; top: 0;"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Upload pom.xml File for Maven(optional):</label>
            </td>
            <td>
                <label for="uploadpom">
                    <img src="images/uploadbtn.jpg">
                    <input type="file" id="uploadpom" onchange="updateUploadJava();" hidden="hidden" accept=".xml">
                </label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Run Compiler:</label>
            </td>
            <td>
                <input id="assignbut" class="assignbut" type="button" value="Compile" onclick="compileCode()">
                <button id="jarMove" hidden="hidden">Move to jars</button>
                <a id="jarDownload" hidden="hidden" href="#">Download</a>
            </td>
        </tr>
    </table>
    <div id="window" hidden="hidden">
        <div id="toolbar">
            <div class="top">
                <div id="lights">
                    <div class="light red" onclick="showCompTerminal(false)" >
                        <div class="glyph">×</div>
                        <div class="shine"></div>
                        <div class="glow"></div>
                    </div>

                </div>
                <div id="title">
                    <div class="folder">
                        <div class="tab"></div>
                        <div class="body"></div>
                    </div>
                    Spark
                </div>
                <div id="bubble">
                    <div class="shine"></div>
                    <div class="glow"></div>
                </div>
            </div>
        </div>
        <div id="consolebody">
            <p class="console-info">Loading...</p>
            <div class="cursor"></div>
        </div>
    </div>
</div>

