<?php

session_start();
require_once "hdfs.class.php";
require_once "user.class.php";

try {
    $user = new User();
    $hdfs = new Hdfs();


    $result = array();

    if (isset($_POST["preview"]))
        echo htmlspecialchars($hdfs->catFile($_POST["preview"]));

    else if(isset($_POST["list"])) {
        $user_path = '/user/' . $user->username;

        $path = "/";

        if($user->level != 0) {
            if (substr($_POST["list"], strlen($user_path)) != $user_path) {
                $path = $user_path;
            }
        }
        echo json_encode($hdfs->listDirectory($path), JSON_UNESCAPED_SLASHES);

    }else if(isset($_POST["move"]) && isset($_POST["src"]) && isset($_POST["dst"])){

        try {
            $hdfs->moveFile($_POST["src"], $_POST["dst"]);
            $result["Result"] = "ok";
        }catch(RuntimeException $ex){
            $result["Result"] = "failed";
            $result["Error"] = $ex->getMessage();
        }
        echo json_encode($result);

    }else if(isset($_POST["delete"])){

        if($hdfs->rmFile($_POST["delete"]) === true)
            $result["Result"] = "ok";
        else
            $result["Error"] = "Failed to delete file...";
        echo json_encode($result);
    }else if(isset($_POST["makedir"])) {

        if ($hdfs->makeDir($_POST["makedir"]) === true)
            $result["Result"] = "ok";
        else
            $result["Error"] = "Failed to delete file...";
        echo json_encode($result);
    }else if(isset($_POST["upload"])) {
        $_SESSION["uploaddst"] = $_POST["upload"];
    }
}
catch(RuntimeException $ex){
    echo $ex->getMessage();
}