function showLoading(show, message){
    if(show) {
        document.getElementById("loading").style.display = "block";
        document.querySelector("#mainbody").className = "disabled";;
    }else {
        document.getElementById("loading").style.display = "none";
        document.querySelector("#mainbody").className = "";;
    }

    if(message == undefined)
        document.getElementById("loading").innerHTML = "<img src='images/throbber.gif'> Loading... Please Wait!";
    else
        document.getElementById("loading").innerHTML = "<img src='images/throbber.gif'>" + message;
}

function hidePopup(){
    document.getElementById("browser").className = "";
    document.getElementById('popup').style.display = 'none';
}

function logoutApp(event){
    event.preventDefault();
    document.getElementById("logoutForm").submit();
}

function navigator(action, postData) {
    if(action == undefined || action==null)
    {
        alert("Undefined action...");
        return;
    }

    formData = new FormData;

    document.querySelector(".active").className = document.querySelector(".active").className.replace(/\bactive\b/, '');
    if (action == 'browseHDFS') {
        file = 'browse_hdfs.php';
        after = window['loadHdfsTree'];
        document.getElementById("menuhdfs").className += ' active';

    } else if (action == 'CompileCode') {
        document.getElementById("menuspark").className += ' active';
        file = 'spark_compiler.php';

    } else if (action == 'AssignJob') {
        document.getElementById("menuspark").className += ' active';
        file = 'spark_assign.php';

    }else if (action == 'MonitorJob') {
        document.getElementById("menuspark").className += ' active';
        if(postData != undefined ){
            formData.append("appid", postData);
        }
        file = 'spark_monitor.php';
    }else
    {
        alert("Undefined action...");
        return;
    }

    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            mainbody.innerHTML =  xmlhttp.responseText;
            showLoading(false);
            if(after != null)
                after();
        }
    }



    showLoading(true);
    xmlhttp.open("POST", file, true);
    xmlhttp.send(formData);
}

function updateTextInput(val) {
    val.nextSibling.innerHTML= val.value;
}

//Kill App
function killApp(appid){
    showLoading(true, "Killing app...");
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = JSON.parse(xmlhttp.responseText);
            showLoading(false);

            if(json["Result"] == "ok"){
                alert("Application killed successfully!");
                navigator("MonitorJob", appid);
            }else{
                alert(json["Error"]);
            }
        }
    }

    formData = new FormData();
    formData.append("appid", appid);
    xmlhttp.open("POST","spark_kill_app.php",true);
    xmlhttp.send(formData);
}

function showTerminal(appid){

    if(document.getElementById("window").hidden){
        document.getElementById("window").hidden = false;
        document.getElementById("console_button").innerHTML = "Hide Terminal";

        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.querySelector("#consolebody").innerHTML = xmlhttp.responseText;
                document.querySelector('#consolebody').scrollTop = '100000000';
            }
        }

        formData = new FormData();
        formData.append("appid", appid);
        xmlhttp.open("POST","spark_console.php",true);
        xmlhttp.send(formData);
    }
    else {
        document.getElementById("console_button").innerHTML = "Show Terminal";
        document.getElementById("window").hidden = true;
    }
}

function changeMonitorData(appid){
    navigator("MonitorJob", appid)
}


function preview(id) {
    document.getElementById("browser").className = "disabled";

    document.getElementById('popup').style.display = 'block';
    var popup = document.getElementById('popup-content');
    showLoading(true);
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            showLoading(false);
            popup.innerHTML = "<pre>" + xmlhttp.responseText + "</pre>";
        }
    };

    formData = new FormData();
    formData.append('preview', id);
    xmlhttp.open("POST","hdfs_functions.php",true);
    xmlhttp.send(formData);
}



var parentUpload = '';

function loadHdfsTree(){
    showLoading(true);
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var fromNode = '';
    var fromParent = '';

    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = JSON.parse(xmlhttp.responseText);
            showLoading(false);
            json["core"].themes = {"stripes": true};
            json["core"].multiple = false;
            json["core"].check_callback = function(operation, node, node_parent, node_position, more) {
                // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
                // in case of 'rename_node' node_position is filled with the new node name

                if (operation === "move_node") {
                    return node_parent.original.type === "folder"; //only allow dropping inside nodes of type 'folder'
                }

                return true;  //allow all other operations
            };


            json["plugins"] = ['contextmenu', 'grid', 'themes', 'types', 'dnd'];

            json["types"] = {
                "default" : {
                },
                "root":{

                },
                "folder" : {
                },
                "file" :{
                    "icon" : "/hdspark/images/files.png"
                }
            };

            json["dnd"] = {
                "drop_check" : function (data) { return false; }
            }

            json["contextmenu"] = {
                "items": function (node) {
                    return {
                        "Preview_Content": {
                            "label": "Preview Content",
                            "action": function (obj) {
                                if(node.type == "file") {
                                    preview(node.id);
                                }else{
                                    alert("Cannot preview directory contents... Just open it if you want!!!");
                                }
                            }
                        },
                        "Download": {
                            "label": "Download",
                            "action": function (obj) {
                                if(node.type == "file"){
                                    window.location = 'download.php?file=' + node.id;
                                }else{
                                    alert("Cannot download folders!! Operation not supported...\nSorry :( :( :(");
                                }
                            }
                        },
                        "Delete": {
                            "label": "Delete",
                            "action": function(obj){
                                deleteNode(node.id);
                            }
                        },
                        "Rename": {
                            "label": "Rename",
                            "action": function (obj) {
                                var newName = prompt("Please enter a new name:");
                                if(newName == null) return;
                                var newPath = node.id.substring(0,node.id.lastIndexOf("/")+1) + newName;
                                moveNode(node.id, newPath);

                            }
                        },
                        "Create_Dir": {
                            "label": "Create Directory",
                            "action": function(obj){
                                var name = prompt("Enter directory name:");
                                if(name == null) return;
                                var newPath;
                                if(node.type == 'folder')
                                    newPath = node.id + '/' + name;
                                else
                                    newPath = node.id.substring(0,node.id.lastIndexOf("/")+1) + name;

                                createDir(newPath);
                            }
                        },
                        "Upload File": {
                            "label": "Upload File",
                            "action": function(obj){
                                if(node.type == 'folder')
                                    parentUpload = node.id + '/';
                                else
                                    parentUpload = node.id.substring(0,node.id.lastIndexOf("/")+1);

                                document.querySelector("#fileUpload").click();
                            }
                        }
                    };
                }
            }
            $('#jstree').jstree(json);

            $(document).on('dnd_start.vakata', function (e, data) {
                node = data.data.origin.get_node(data.data.nodes[0]);

                fromNode= node.id;
                fromParent = node.parent;

            });

            $(document).on('dnd_stop.vakata', function (e, data) {
                node = data.data.origin.get_node(data.data.nodes[0]);

                if(node.parent == fromParent){
                    console.log("cannot move to same directory... cancel");
                    return false
                }
                var t = $(data.event.target);
                var targetnode = t.closest('.jstree-node');
                var targetId = targetnode.attr("id");

                console.log('mv '+ fromNode + ' ' + targetId);

                moveNode(fromNode, targetId);
            });
        }
    };

    formData = new FormData();
    formData.append('list', '~');
    xmlhttp.open("POST","hdfs_functions.php",true);
    xmlhttp.send(formData);
}

function moveNode(src, dst){
    showLoading(true);
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = JSON.parse(xmlhttp.responseText);
            showLoading(false);
            if(json["Result"] == "ok")
                navigator("browseHDFS");
            else{
                alert(json["Error"]);
                navigator("browseHDFS");
            }
        }
    }

    formData = new FormData();
    formData.append('move', 'move');
    formData.append('src', src);
    formData.append('dst', dst);
    xmlhttp.open("POST","hdfs_functions.php",true);
    xmlhttp.send(formData);
}

function deleteNode(src){
    showLoading(true);
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = JSON.parse(xmlhttp.responseText);
            showLoading(false);
            if(json["Result"] == "ok")
                navigator("browseHDFS");
            else{
                alert(json["Error"]);
                navigator("browseHDFS");
            }
        }
    }

    formData = new FormData();
    formData.append('delete', src);
    xmlhttp.open("POST","hdfs_functions.php",true);
    xmlhttp.send(formData);
}

function createDir(src){
    showLoading(true);
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = JSON.parse(xmlhttp.responseText);
            showLoading(false);
            if(json["Result"] == "ok")
                navigator("browseHDFS");
            else{
                alert(json["Error"]);
                navigator("browseHDFS");
            }
        }
    }

    formData = new FormData();
    formData.append('makedir', src);
    xmlhttp.open("POST","hdfs_functions.php",true);
    xmlhttp.send(formData);
}


function UploadFile() {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            sendFile();
        }
    }

    var uploader = document.querySelector("#fileUpload");
    var fileName = uploader.value;
    if(fileName.indexOf('\\') != -1)
        fileName = fileName.substr(fileName.lastIndexOf('\\') + 1);


    formData = new FormData();
    formData.append('upload', parentUpload + fileName);
    xmlhttp.open("POST","hdfs_functions.php",true);
    xmlhttp.send(formData);
}

function sendFile(){
    var uploader = document.querySelector("#fileUpload");
    var file = uploader.files[0];
    var reader = new FileReader();
    reader.readAsBinaryString(file);

    reader.onloadend = function (evt) {
        // create XHR instance
        xhr = new XMLHttpRequest();

        // send the file through POST
        xhr.open("POST", 'hdfs_upload.php', true);

        // make sure we have the sendAsBinary method on all browsers
        XMLHttpRequest.prototype.mySendAsBinary = function (text) {
            var data = new ArrayBuffer(text.length);
            var ui8a = new Uint8Array(data, 0);
            for (var i = 0; i < text.length; i++) ui8a[i] = (text.charCodeAt(i) & 0xff);

            if (typeof window.Blob == "function") {
                var blob = new Blob([data]);
            } else {
                var bb = new (window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder)();
                bb.append(data);
                var blob = bb.getBlob();
            }

            this.send(blob);
        }

        // let's track upload progress
        var eventSource = xhr.upload || xhr;
        eventSource.addEventListener("progress", function (e) {
            // get percentage of how much of the current file has been sent
            var position = e.position || e.loaded;
            var total = e.totalSize || e.total;
            var percentage = Math.round((position / total) * 100);

            showLoading(true, "Uploading... " + percentage + "%");

        });

        // state change observer - we need to know when and if the file was successfully uploaded
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                } else {
                    alert("ERROR!!!\nxhr status: " + xhr.status);
                }
                showLoading(false);
                navigator('browseHDFS');
            }
        };

        // start sending
        xhr.mySendAsBinary(evt.target.result);
    };
}

function uploadJar() {
    var file = document.getElementById("uploadjar");
    if (file.files.length === 0) {
        return;
    }

    var data = new FormData();
    data.append('type', 'jar')
    data.append('SelectedFile', file.files[0]);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            showLoading(false);
            var json = JSON.parse(xmlhttp.responseText);
            if(json["Result"] == "ok") {
                navigator("AssignJob");
            }else{
                alert("Upload failed... :(\nError: " + json["Error"]);
            }
        }
    };

    xmlhttp.upload.addEventListener('progress', function (e) {
       showLoading(true, "Uploading...");
    }, false);

    xmlhttp.open('POST', 'spark_upload.php');
    xmlhttp.send(data);
}

function showCompTerminal(show){
    document.getElementById("window").hidden = !show;
}

function runJob(){
    var cores = document.getElementById("core_selector").value;
    var memory = document.getElementById("memory_selector").value;
    var jar = document.getElementById("jar_selector").value;
    var params = document.getElementById("cmdline_parameters").value;
    var start = document.getElementById("main_class").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            showLoading(false);
            console.log(xmlhttp.responseText);
            var json = JSON.parse(xmlhttp.responseText);
            if(json["Result"] == "ok") {
                alert("Job started successfully!!! (yeah)")
                navigator("MonitorJob", json["id"]);
            }else{
                alert("Starting failed... :(\nError: " + json["Error"]);
            }
        }
    };

    if(jar == ""){
        alert("Please select or upload a Jar file.");
        return;
    }
    if(start == ""){
        alert("Please enter the Class of main() function.");
        return;
    }


    var data = new FormData();
    data.append('cores', cores);
    data.append('memory', memory);
    data.append('jar', jar);
    data.append('params', params);
    data.append('class', start);

    showLoading(true, "Starting Spark job...<br>This will take a while!!!");

    xmlhttp.open('POST', 'spark_run_job.php');
    xmlhttp.send(data);
}

function compileCode(){
    var source = document.getElementById("uploadsource");
    var pom = document.getElementById("uploadpom");

    var files = source.files;

    if (files.length === 0) {
        return;
    }

    var data = new FormData();
    data.append('type', 'source')

    // Loop through each of the selected files.
    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // Add the file to the request.
        data.append('sources[]', file, file.name);
    }

    if(pom.files.length !== 0)
        data.append('pom', pom.files[0], pom.files[0].name);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            showLoading(false);
            console.log(xmlhttp.responseText);
            var json = JSON.parse(xmlhttp.responseText);
            if(json["Result"] == "ok"){
                document.getElementById("window").hidden = false;
                document.querySelector("#consolebody").innerHTML = json["compile"];

                if(json["Jar"] != undefined){
                    document.getElementById("assignbut").hidden = true;
                    document.getElementById("jarDownload").hidden = false;
                    document.getElementById("jarDownload").href = json["Jar"];
                    document.getElementById("jarMove").hidden = false;
                    document.getElementById("jarMove").onclick = function(){moveToJars(json["Jar"]);}
                }
            }else{
                alert("Upload and compile failed...\nError: " + json["Error"]);
            }
        }
    };

    showLoading(true, "Compiling sources...");

    xmlhttp.open('POST', 'spark_upload.php');
    xmlhttp.send(data);
}

function updateUploadJava(){
    var sources = document.getElementById("uploadsource").files;
    var pom = document.getElementById("uploadpom").files;

    var text = '';
    for(var i=0; i<sources.length; ++i)
        text += sources[i].name + "<br>";

    if(pom.length !==0)
        text += pom[0].name + "<br>";

    document.getElementById("javafiles").innerHTML = text;
}

function moveToJars($path){
    showLoading(true, "Moving jar file...");
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            showLoading(false);
            var json = JSON.parse(xmlhttp.responseText);
            if(json["Result"] == "ok") {
                alert("Moved successfully!");
            }else{
                alert("Moving failed... :(\nError: " + json["Error"]);
            }
        }
    };

    var data = new FormData();
    data.append('type', 'move')
    data.append('jar', $path)
    xmlhttp.open('POST', 'spark_upload.php');
    xmlhttp.send(data);
}