<?php

require "user.class.php";
session_start();
$user = new User();

if(!$user->isLoggedIn())
    $user->redirectTo('login');
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="dist/themes/default/style.css" />
    <link rel="icon"
          type="image/png"
          href="favicon-16x16.png" />

    <script src="script.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="dist/jstree.min.js"></script>
    <title>HDSpark</title>

</head>
<body onload="navigator('browseHDFS')">


<div id='navigationbar'>
    <ul>
            <li id="menuhdfs" class='active left'><a href='#' onclick="navigator('browseHDFS');"><span>HDFS File Explorer</span></a></li>
            <li id="menuspark" class='has-sub left'><a><span>Spark</span></a>
                <ul>
                    <li><a href='#' onclick="navigator('CompileCode');"><span>Compile Code</span></a></li>
                    <li><a href='#' onclick="navigator('AssignJob');"><span>Assign Job</span></a></li>
                    <li><a href='#' onclick="navigator('MonitorJob');"><span>Monitor Job</span></a></li>
                </ul>
            </li>
            <li class='right'>
                <?php echo $user->username; ?>
                <form method="post" action="index.php" id="logoutForm">
                    <input type="hidden" name="logout" value="logout">
                    <a href="#" onclick="logoutApp(event);">Sign Out</a>
                </form>
            </li>

    </ul>
</div>
<div id="mainbody">

</div>
<div id="loading">
    <img src="images/throbber.gif"> Loading... Please Wait!
</div>
<?php include "footer.html" ?>

</body>
</html>

