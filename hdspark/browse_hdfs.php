<div id="browser">
    <img id="hdfslogo" src="images/hdfs-logo.png" alt="HDFS">
    <div id="jstree"></div>
</div>
<div id="popup" class="popup">
    <div><button onclick="hidePopup();" id="popupClose">Close</button></div>
    <div id="popup-content"></div>
</div>
<input type="file" hidden="hidden" id="fileUpload" name="fileUpload" onchange="UploadFile()">