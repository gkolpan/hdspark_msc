<?php
class Spark{
    function __construct(){}

    function run($jar, $parameters){

        include_once("user.class.php");
        $user = new User();

        if(!$user->isLoggedIn())
            throw new RuntimeException("User is not logged in");


        date_default_timezone_set('Europe/Athens');
        $folder = 'executors/' . $user->username . '/' . date("Ymd_His");;
        $time =  gmdate("Y-m-d") . 'T' . gmdate("H:i:s");
        $file = $folder . '/' . $jar;
        //mkdir in executors
        if(!mkdir($folder, 0777, true))
            throw new RuntimeException("mkdir failed...");

        //cp jar from jars in new dir
        if(!copy('jars/' . $user->username . '/' . $jar, $file))
            throw new RuntimeException("copy failed...");

        //copy run.sh
        if(!copy('executors/run.sh', $folder . '/run.sh'))
            throw new RuntimeException("copy of run.sh failed...");

        //change dir
        if(!chdir ( $folder ))
            throw new RuntimeException("Failed to change to folder...");

        //make run.sh executable
        if(!chmod('run.sh', 0555))
            throw new RuntimeException("Failed to make run.sh executable...");

        if(!chmod('.', 0777))
            throw new RuntimeException("Failed to make folder writable");

        //nohup spark-submit using ./run.sh
        $command = 'sudo -u hduser ./run.sh ';
        $command .= $parameters["memory"] . 'm ';
        $command .= $parameters["cores"] . ' ';
        $command .= $parameters["class"] . ' ';
        $command .= ' ' . $jar;

        if(isset($parameters["userparams"]))
            $command .= ' ' . $parameters["userparams"];

        $pid = exec("$command 2>&1");

        $name = '';
        $started = '';
        try {
            //get id or throw error
            $counter = 0;
            while (true) {
                if ($counter++ > 120) //2min timeout...
                    throw new RuntimeException("Start time out occured...");

                $errors = exec("cat nohup.out | grep 'Utils: Shutdown hook called'");
                if (strlen($errors) > 0) {
                    throw new RuntimeException("Errors: " . $errors);
                }

                $id = exec("cat nohup.out | grep 'Connected to Spark cluster with app ID'");
                if (strlen($id) > 0) {
                    $id = substr($id, strpos($id, 'app ID') + 7, 23);
                    break;
                }

                //wait 1sec before retrying
                sleep(1);
            }

            sleep(10); //wait for spark to write history logs...(mlkia!!!)
            //get app name
            $data = json_decode(file_get_contents("http://sparkmaster:18080/api/v1/applications/$id"), true);
            if (!isset($data["name"]))
                throw new RuntimeException("Failed to fetch app name...");
            $name = $data["name"];
            $started = $data["attempts"][0]["startTime"];
        }
        catch(RuntimeException $ex){
            // failed to catch app name and id
            if(!isset($id))
                $id = "failed-$time";

            $name = "Unknown...";
            $started = $time;
        }


        include "config.php";
        $sql = $conn->prepare("INSERT INTO `web_hdspark`.`jobs` (`appid`,`folder`,`user`,`appName`,`dateStarted`, `process`, `memory`, `cores`) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");

        if(!$sql)
            throw new RuntimeException("conn failed");

        $sql->bind_param("ssisssss", $id, $folder, $user->id, $name, $started, $pid, $parameters["memory"], $parameters["cores"]);

        $res = $sql->execute();

        if(!$res) {
            throw new RuntimeException("Input to db failed...\n$sql->error()");
        }
        $sql->close();

        return $id;
    }

    function upload(){

    }

    function getUserApps(){
        require "config.php";
        require_once "user.class.php";

        $user = new User();

        $sql = $conn->prepare("SELECT `jobs`.`appid`, `jobs`.`folder`, `jobs`.`appName`, `jobs`.`dateStarted`, `jobs`.`memory`, `jobs`.`cores`, `jobs`.`process` FROM `web_hdspark`.`jobs` WHERE `jobs`.`user` = ? ORDER BY `jobs`.`appid` DESC;");

        if(!$sql)
            throw new RuntimeException("Unable to create query get jobs");

        $sql->bind_param("i", $user->id);
        $res = $sql->execute();

        if (!$res)
            throw new RuntimeException('Unable to run query get users...');

        $sql->store_result();
        $sql->bind_result($appid, $folder, $appName, $dateStarted, $memory, $cores, $process);
        $results = array();
        for($i=0; $sql->fetch(); ++$i)
        {
            $results[$i]["appid"] = $appid;
            $results[$i]["folder"] = $folder;
            $results[$i]["name"] = $appName;
            $results[$i]["date"] = $dateStarted;
            $results[$i]["memory"] = $memory;
            $results[$i]["cores"] = $cores;
            $results[$i]["process"] = $process;
        }

        $sql->close();
        $conn->close();

        return $results;
    }

    function getAppDetails($appid){
        if(!isset($appid))
            throw new RuntimeException("Unable to continue without appid...");

        $result = array();

        $data = json_decode(file_get_contents("http://sparkmaster:18080/api/v1/applications/$appid"), true);

        if(!isset($data["id"]))
            throw new RuntimeException("ID not found... :(");

        $result["name"] = $data["name"];
        $result["startTime"] = $data["attempts"][0]["startTime"];
        $result["endTime"] = $data["attempts"][0]["endTime"];
        $result["status"] = $data["attempts"][0]["completed"] == true ? 'FINISHED' : 'PENDING';

        if($data["attempts"][0]["completed"] == true) {
            $data = json_decode(file_get_contents("http://sparkmaster:18080/api/v1/applications/$appid/stages"), true);
        }else {
            $data = json_decode(file_get_contents("http://sparkmaster:4040/api/v1/applications/" . rawurlencode($result["name"]) . "/stages"), true);
        }

        for($i=0; $i<count($data); $i++)
        {
            $result["stages"][$i]["name"] = $data[$i]["name"];
            $result["stages"][$i]["stageId"] = $data[$i]["stageId"];
            $result["stages"][$i]["time"] = $data[$i]["executorRunTime"];
            $result["stages"][$i]["shuffleReadRecords"] = $data[$i]["shuffleReadRecords"];
            $result["stages"][$i]["inputRecords"] = $data[$i]["inputRecords"];
            $result["stages"][$i]["outputRecords"] = $data[$i]["outputRecords"];
            $result["stages"][$i]["shuffleWriteRecords"] = $data[$i]["shuffleWriteRecords"];
            $result["stages"][$i]["status"] = $data[$i]["status"];
        }

        return $result;
    }

    function getUserQuotas(){
        require_once "config.php";
        require_once "user.class.php";

        $user = new User();
        $sql = $conn->prepare("SELECT `user_quotas`.`memory`, `user_quotas`.`cores` FROM `web_hdspark`.`user_quotas` WHERE `web_hdspark`.`user_quotas`.`user` = ?;");

        if(!$sql)
            throw new RuntimeException("Unable to create query get jobs\n" .$conn->error);

        $sql->bind_param("i", $user->id);
        $res = $sql->execute();

        if (!$res)
            throw new RuntimeException('Unable to run query get users...');

        $sql->store_result();
        $results = array();
        $sql->bind_result($results["memory"], $results["cores"]);
        $sql->fetch();

        $sql->close();
        $conn->close();

        $apps = $this->getUserApps();
        foreach($apps as $app) {
            if (strpos($app["appid"], "failed") === false) {
                // commited apps with appid
                $data = json_decode(file_get_contents("http://sparkmaster:18080/api/v1/applications/". $app["appid"]), true);

                if (!isset($data["id"]))
                    continue;

                if ($data["attempts"][0]["completed"] != true) {

                    $results["memory"] -= $app["memory"];
                    $results["cores"] -= $app["cores"];
                }
            }else {
                // commited apps without appid
                exec("sudo -u hduser ps " . $app["process"], $out, $res);
                if($res == '0'){
                    //keeps running
                    $results["memory"] -= $app["memory"];
                    $results["cores"] -= $app["cores"];
                }
            }
        }


        return $results;
    }

    function getUserJars(){
        require_once "user.class.php";
        $user = new User();

        exec("ls jars/$user->username", $res);
        return $res;
    }

    function compile($path){
        include_once("user.class.php");
        $user = new User();

        if(!$user->isLoggedIn())
            throw new RuntimeException("User is not logged in");

        //cp compile.sh
        if(!copy('compiles/compile.sh', $path . '/compile.sh'))
            throw new RuntimeException("copy failed...");

        //change dir
        if(!chdir ( $path ))
            throw new RuntimeException("Failed to change to folder...");

        //make compile.sh executable
        if(!chmod('compile.sh', 0755))
            throw new RuntimeException("Failed to make run.sh executable...");

        exec('./compile.sh', $return);

        return $return;
    }

}
?>