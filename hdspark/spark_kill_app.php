<?php

session_start();

require "user.class.php";
require "config.php";

if(!isset($_POST["appid"]))
    throw new RuntimeException("There is no app id to show... :(");

$user = new User();
if(!$user->isLoggedIn())
    $user->redirectTo('login');

$sql = $conn->prepare("SELECT `jobs`.`process` FROM `web_hdspark`.`jobs` WHERE `jobs`.`appid` = ?;");

if(!$sql)
    throw new RuntimeException("Unable to create query get jobs");

$sql->bind_param("s", $_POST["appid"]);

$res = $sql->execute();

if (!$res)
    throw new RuntimeException('Unable to run query get users...');

$sql->store_result();
$sql->bind_result($proc);
$sql->fetch();

exec("sudo -u hduser kill $proc", $out, $res);

$result = array();
if($res == 0)
    $result["Result"] = "ok";
else {
    $result["Result"] = "failed";
    $result["Error"] = "Couldn't kill app... :( Sorry...";
}
$sql->close();
$conn->close();

echo json_encode($result);
?>