<?php
session_start();

require_once "user.class.php";
require_once "spark.class.php";

$user = new User();

if(!isset($_POST["type"]))
    return;

if($_POST["type"] == 'jar') {
    $result = array();

    try {
        if (
            !isset($_FILES['SelectedFile']['error']) ||
            is_array($_FILES['SelectedFile']['error'])
        ) {
            throw new RuntimeException('Invalid parameters.');
        }

        // Check $_FILES['upfile']['error'] value.
        switch ($_FILES['SelectedFile']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.');
            default:
                throw new RuntimeException('Unknown errors.');
        }

        // You should also check filesize here. (100MB)
        if ($_FILES['SelectedFile']['size'] > 100000000) {
            throw new RuntimeException('Exceeded filesize limit.');
        }

        if($_FILES['SelectedFile']['type'] != 'application/x-java-archive')
            throw new RuntimeException('Unsupported format: ' . $_FILES['SelectedFile']['type']);


        $uploadfile = 'jars/' . $user->username . '/' . $_FILES['SelectedFile']['name'];

        if (file_exists($uploadfile)) {
            if (!unlink($uploadfile))
                throw new RuntimeException("Failed to delete same file...");
        }

        if (!move_uploaded_file($_FILES['SelectedFile']['tmp_name'], $uploadfile)) {
            throw new RuntimeException('Failed to move uploaded file.');
        }

        $result["Result"] = "ok";
    }catch(RuntimeException $ex){
        $result["Resultult"] = "failed";
        $result["Error"] = $ex->getMessage();
    }

}else if($_POST["type"] == 'source'){
    $result = array();

    try {
        date_default_timezone_set('Europe/Athens');
        $folder = 'compiles/' . $user->username . '/' . date("Ymd_His");;

        //mkdir in executors
        if(!mkdir($folder, 0755, true))
            throw new RuntimeException("mkdir failed...");

        for($i=0; $i<count($_FILES['sources']['name']); ++$i) {
            if (
                !isset($_FILES['sources']['error'][$i]) ||
                is_array($_FILES['sources']['error'][$i])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['upfile']['error'] value.
            switch ($_FILES['sources']['error'][$i]) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here. (100MB)
            if ($_FILES['sources']['size'][$i] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            if ($_FILES['sources']['type'][$i] != 'text/x-java')
                throw new RuntimeException('sources... Unsupported format: ' . $_FILES['sources']['type'][$i]);

            $uploadfile = $folder . '/' . $_FILES['sources']['name'][$i];

            if (!move_uploaded_file($_FILES['sources']['tmp_name'][$i], $uploadfile)) {
                throw new RuntimeException('Failed to move uploaded file.');
            }
        }

        if(isset($_FILES["pom"])){
            if (
                !isset($_FILES['pom']['error']) ||
                is_array($_FILES['pom']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['upfile']['error'] value.
            switch ($_FILES['pom']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here. (1MB)
            if ($_FILES['pom']['size'] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            if ($_FILES['pom']['type'] != 'text/xml')
                throw new RuntimeException('Unsupported format: ' . $_FILES['pom']['type']);

            $uploadfile = $folder . '/pom.xml';

            if (!move_uploaded_file($_FILES['pom']['tmp_name'], $uploadfile)) {
                throw new RuntimeException('Failed to move uploaded file.');
            }
        }

        $spark = new Spark();
        $comp = $spark->compile($folder);

        $result["Result"] = "ok";

        $colored = '';
        foreach($comp as $l){
            if(strpos($l, '[INFO] BUILD SUCCESS') !== false)
                $colored .= "<p class='console-info'>$l</p>";
            else if(strpos($l, '[ERROR]') !== false)
                $colored .= "<p class='console-error'>$l</p>";
            else if(strpos($l, '[INFO]') !== false)
                $colored .= "<p class='console-none'>$l</p>";
        }
        $result["compile"] = $colored;

        // find if jar is there...
        $list = glob('target/*.jar');
        if(count($list)>0)
            $result["Jar"] = $folder . '/' . $list[0];

    }catch(RuntimeException $ex){
        $result["Result"] = "failed";
        $result["Error"] = $ex->getMessage();
    }

}else if($_POST["type"] == 'move') {
  try{
      date_default_timezone_set('Europe/Athens');

      $source = $_POST["jar"];
      if(strpos($source,'compiler_out') === false)
          $destination = 'jars/' . $user->username . substr($source, strrpos($source, '/'));
      else
          $destination = 'jars/' . $user->username . '/compile_' . date("Ymd_His") . '.jar';

      if (file_exists($destination)) {
          if (!unlink($destination))
              throw new RuntimeException("Failed to delete same file...");
      }

      if (!copy($source, $destination)) {
          throw new RuntimeException('Failed to move uploaded file.');
      }

      $result["Result"] = "ok";
  }catch(RuntimeException $ex){
      $result["Resultult"] = "failed";
      $result["Error"] = $ex->getMessage();
  }

}
echo json_encode($result);

?>