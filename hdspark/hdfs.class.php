<?php

class Hdfs{

	function __construct(){}
	
   	function listDirectory($path){

        $json["core"] = ["data" => array()];

        $json["core"]["data"] = array([
                "id" => $path,
                'text' => $path,
                'state' => ['opened' => true, 'selected' => true],
                'type' => "folder"
        ]);

        //recursively go into directories!!!
        $json["core"]["data"][0]["children"] = $this->getDirectoryList($path);
		return $json;
	}

    private function getDirectoryList($path){
        $command = 'http://sparkmaster:50070/webhdfs/v1' . $path . '?op=LISTSTATUS';
        $resp = json_decode(file_get_contents($command), true);

        $files = &$resp["FileStatuses"]["FileStatus"];

        $ret = array();

        if(strrpos($path, '/') == strlen($path) -1)
            $path = '';

        for($i=0; $i<count($files); $i++)
        {
            $text = $files[$i]["pathSuffix"];
            $id = $path . '/' . $files[$i]["pathSuffix"];
            $type = $files[$i]["type"];

            $ret[$i] = [
                'id' => $id,
                'text' => $text,
            ];

            if($type == 'FILE') {
                $ret[$i]['text'] .= "<span style='float:right'>" . $files[$i]["length"]/1000 . "KB</span>";
                $ret[$i]['type'] = 'file';
            }else{
                //echo 'adding folder ' . $id . '<br>';
                $ret[$i]['type'] = 'folder';
                $ret[$i]['children'] = $this->getDirectoryList($id);
            }
        }

        return $ret;
    }
	
	function catFile($path){
        return file_get_contents("http://sparkmaster:50070/webhdfs/v1" . $path . "?op=OPEN&length=10000");
	}

    function makeDir($path){
        ///webhdfs/v1". ."?user.name=". $user->username ."&op=MKDIRS

        $user = new User();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,"http://sparkmaster:50070/webhdfs/v1" . $path . "?user.name=". $user->username ."&op=MKDIRS");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl), true);

        curl_close($curl); if(array_key_exists("RemoteException", $result))
            throw new RuntimeException(var_dump($result["RemoteException"]["message"]));

        return $result["boolean"];
    }

    function addFile($path){
        $user = new User();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,"http://sparkmaster:50070/webhdfs/v1" . $path . "?user.name=". $user->username ."&op=CREATE");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);

        $response = curl_exec($curl);
        //echo $response;
        preg_match('/^Location:(.*)$/mi', $response, $match);
        curl_close($curl);

        $newCommand = substr($match[0], 10, strlen($match[0])-11);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $newCommand);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        //echo json_encode($response);

        //ERROR
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,"http://sparkmaster:50070/webhdfs/v1" . $path . "?user.name=". $user->username ."&op=APPEND");
        curl_setopt($curl, CURLOPT_POST,1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);

        $response = curl_exec($curl);
        preg_match('/^Location:(.*)$/mi', $response, $match);
        curl_close($curl);
        echo "starting pipe...";

        $newCommand = substr($match[0], 10, strlen($match[0])-11);

        $descriptorspec = array(
            0 => array("pipe", "r"),  // stdin
            1 => array("pipe", "w"),  // stdout
            2 => array("pipe", "w")   // stderr
        );

        $command = 'curl -i -X POST -T - "' . $newCommand . '"';

        echo "command: " . $command;
        $process = proc_open($command, $descriptorspec, $pipes);

        if (!is_resource($process))
            throw new RuntimeException('Error: Failed to open pipe in Hdfs::upload()');



        $fh = fopen("php://input", 'r');

        while(true) {
            $buffer = fgets($fh, 4096);
            if (strlen($buffer) == 0) {
                fclose($fh);
                fclose($pipes[0]);
                return true;
            }

            echo $buffer;
            fwrite($pipes[0], $buffer);
        }

        //return true;
    }

    function rmFile($path){
        $user = new User();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,"http://sparkmaster:50070/webhdfs/v1" . $path . "?user.name=". $user->username ."&op=DELETE&recursive=true");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl), true);
        curl_close($curl);

        if(array_key_exists("RemoteException", $result))
            throw new RuntimeException(var_dump($result["RemoteException"]["message"]));

        return $result["boolean"];
    }

    function downloadFile($path){
        //check if file exists...
        $command = '/usr/local/hadoop/bin/hdfs dfs -test -e ' . escapeshellarg($path) . ' 2>&1';
        exec($command, $result, $ret);

        if($ret != '0')
            throw new RuntimeException('Error: File not found... :(');

        $command = '/usr/local/hadoop/bin/hdfs dfs -cat '. escapeshellarg($path);

        $descriptorspec = array(
            0 => array("pipe", "r"),  // stdin
            1 => array("pipe", "w"),  // stdout
            2 => array("pipe", "w")   // stderr
        );

        $process = proc_open($command, $descriptorspec, $pipes);

        if (!is_resource($process))
            throw new RuntimeException('Error: Failed to open pipe in Hdfs::catFile()');

        $errors = stream_get_contents($pipes[2]); // stderr
        if($errors)
            throw new RuntimeException('Error: ' . substr($errors, strpos($errors, ':') + 2));
        fclose($pipes[2]);

        $filename = substr($path, strrpos($path, '/') + 1);

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: application/binary");
        header("Content-Disposition: attachment; filename={$filename}");
        header("Expires: 0");
        header("Pragma: public");

        $fh = @fopen( 'php://output', 'w' );


        while($read = fread($pipes[1], 2096))
            fwrite($fh, $read);

        fclose($fh);
        fclose($pipes[1]);
    }

    function moveFile($src, $dst){
        $user = new User();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "http://sparkmaster:50070/webhdfs/v1" . $src . "?user.name=". $user->username ."&op=RENAME&destination=" . $dst);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl), true);
        curl_close($curl);
        if(array_key_exists("RemoteException", $result))
            throw new RuntimeException(var_dump($result["RemoteException"]["message"]));

        return $result["boolean"];

    }
}

?>